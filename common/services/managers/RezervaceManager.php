<?php

namespace common\services\managers;

use common\models\dao\IRezervaceDAO;
use yii\base\Object;

/**
 * Manager pro IRezervaceDAO
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:11
 */
class RezervaceManager extends Object
{

    /**
     * Obsluhované DAO
     * @var IRezervaceDAO
     */
    protected $dao;

    /**
     * Vrátí DAO
     * @return IRezervaceDAO Obshluhované DAO
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * Nastaví aktuální DAO (provádí DI)
     * @param IRezervaceDAO $dao
     */
    public function setDao(IRezervaceDAO $dao)
    {
        $this->dao = $dao;
    }

}
