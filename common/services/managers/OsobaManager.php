<?php

namespace common\services\managers;

use common\models\dao\IOsobaDAO;
use yii\base\Object;

/**
 * Manager pro IOsobaDAO
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:11
 */
class OsobaManager extends Object
{

    /**
     * Obsluhované DAO
     * @var IOsobaDAO
     */
    protected $dao;

    /**
     * Vrátí DAO
     * @return IOsobaDAO Obshluhované DAO
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * Nastaví aktuální DAO (provádí DI)
     * @param IOsobaDAO $dao
     */
    public function setDao(IOsobaDAO $dao)
    {
        $this->dao = $dao;
    }

}
