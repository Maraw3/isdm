<?php

namespace common\services\managers;

use common\models\dao\IRezidentDao;
use yii\base\Object;

/**
 * Manager pro IRezidentDAO
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:11
 */
class RezidentManager extends Object
{
    /**
     * Obsluhované DAO
     * @var IRezidentDao 
     */
    protected $dao;
    
    /**
     * Vrátí DAO
     * @return IRezidentDao Obshluhované DAO
     */
    public function getDao()
    {
        return $this->dao;
    }
    
    /**
     * Nastaví aktuální DAO (provádí DI)
     * @param IRezidentDao $dao
     */
    public function setDao(IRezidentDao $dao)
    {
        $this->dao = $dao;
    }
}