<?php

namespace common\services\managers;

use common\models\dao\IPokojDAO;
use yii\base\Object;

/**
 * Manager pro IPokojDAO
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:11
 */
class PokojManager extends Object
{

    /**
     * Obsluhované DAO
     * @var IPokojDAO
     */
    protected $dao;

    /**
     * Vrátí DAO
     * @return IPokojDAO Obshluhované DAO
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * Nastaví aktuální DAO (provádí DI)
     * @param IPokojDAO $dao
     */
    public function setDao(IPokojDAO $dao)
    {
        $this->dao = $dao;
    }

}
