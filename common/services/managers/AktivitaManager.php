<?php

namespace common\services\managers;

use common\models\dao\IAktivitaDAO;
use yii\base\Object;

/**
 * Manager pro IAktivitaDAO
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:11
 */
class AktivitaManager extends Object
{

    /**
     * Obsluhované DAO
     * @var IAktivitaDAO
     */
    protected $dao;

    /**
     * Vrátí DAO
     * @return IAktivitaDAO Obshluhované DAO
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * Nastaví aktuální DAO (provádí DI)
     * @param IAktivitaDAO $dao
     */
    public function setDao(IAktivitaDAO $dao)
    {
        $this->dao = $dao;
    }

}
