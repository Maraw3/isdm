<?php

namespace common\services\operations;

use yii\base\Object;
use common\models\entity\Aktivita;
use common\models\entity\Rezident;
use common\services\managers\RezidentManager;
use common\services\managers\AktivitaManager;

/**
 * Operations pro administrativního pracovníka
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:15:07
 */
class AktivityOperations extends Object implements IAktivityOperations
{

    /**
     * Manažer aktivit
     * @var AktivitaManager
     */
    protected $aktivitaManager;

    /**
     * Manažer rezidentů
     * @var RezidentManager
     */
    protected $rezidentManager;

    /**
     * Přidání nové aktivity.
     * @param Aktivita $aktivita Aktivita k uložení
     * @return boolean True, pokud byl záznam uložen
     */
    public function addAktivita(Aktivita $aktivita)
    {
        if (!$aktivita->validate()) {
            return false;
        }

        return $this->aktivitaManager->getDao()->add($aktivita);
    }

    /**
     * Odstraní aktivitu
     * @param Aktivita $aktivita Aktivita k odstranění
     * @return boolean True, pokud byl záznam odstraněn
     */
    public function deleteAktivita(Aktivita $aktivita)
    {
        return $this->aktivitaManager->getDao()->remove($aktivita);
    }

    /**
     * Vrátí všechny aktivity
     * @return ArrayCollection<Aktivita> Všechny aktivity
     */
    public function getAllAktivita()
    {
        return $this->aktivitaManager->getDao()->getAll();
    }

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezindet> Všichni rezidenti
     */
    public function getAllRezident()
    {
        return $this->rezidentManager->getDao()->getAll();
    }

    /**
     * Vrátí jednu aktivitu podle ID
     * @param integer $id ID aktivity
     * @return Aktivita|null Nalezená aktivita nebo null;
     */
    public function getOneByIdAktivita($id)
    {
        return $this->aktivitaManager->getDao()->getOneById($id);
    }

    /**
     * Vrátí jednodo rezidenta podle jeho ID
     * @param integer $id ID rezidenta
     * @return Rezident|null Nalezený rezident nebo null;
     */
    public function getOneByIdRezident(integer $id)
    {
        return $this->rezidentManager->getDao()->getOneById($id);
    }

    /**
     * Přihlásí rezidenta na danou aktivitu
     * @param Rezident $rezident Rezident který se hlásí na danou aktivitu
     * @param Aktivita $aktivita Aktivita, na kterou se rezident hlásí
     * @return boolean True, pokud byl rezident úspěšně přihlášen na aktivitu
     */
    public function loginToActivity(Rezident $rezident, Aktivita $aktivita)
    {
        try {
            $rezident->link('aktivita', $aktivita);
            return true;
        } catch (\yii\base\InvalidCallException $e) {
            return false;
        }
    }

    /**
     * Uloží upravenou aktivitu
     * @param Aktivita $aktivita Aktivita k uložení
     * @return boolean True, pokud byla aktivita uložena
     */
    public function updateAktivita(Aktivita $aktivita)
    {
        if (!$aktivita->validate()) {
            return false;
        }

        return $this->aktivitaManager->getDao()->update($aktivita);
    }

}
