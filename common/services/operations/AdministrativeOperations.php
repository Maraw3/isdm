<?php

namespace common\services\operations;

use common\models\entity\Rezident;
use common\models\entity\Pokoj;
use common\models\entity\Osoba;
use common\models\entity\Rezervace;
use common\services\managers\PokojManager;
use common\services\managers\RezervaceManager;
use common\services\managers\RezidentManager;
use common\services\managers\OsobaManager;
use yii\base\Object;

/**
 * Operations pro administrativního pracovníka
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:12
 */
class AdministrativeOperations extends Object implements IAdministrativeOperations
{

    /**
     * Manažer pokojů
     * @var PokojManager
     */
    protected $pokojManager;

    /**
     * Manažer rezervací
     * @var RezervaceManager
     */
    protected $rezervaceManager;

    /**
     * Manažer rezidentů
     * @var RezidentManager
     */
    protected $rezidentManager;
    
    /**
     * Manažer rezidentů
     * @var OsobaManager
     */
    protected $osobaManager;

    /**
     * Ubytuje rezidenta na daný pokoj, pokud je to možné (na pokoji je místo)
     * @param Rezident $rezident Rezident k ubytování
     * @param Pokoj $pokoj Pokoj do kterého se má rezident nastěhovat
     * @return boolean Vratí true, pokud bylo ubytování úspěšné. False pokud je pokoj plný
     */
    public function accomodateRezident(Rezident $rezident, Pokoj $pokoj)
    {        
        if (!$rezident->validate()) {
            return false;
        }
        
        $actualCout = $this->pokojManager->getDao()->getCountOfRezident($pokoj->cislo_pokoje);
        
        if ($actualCout >= $pokoj->kapacita) {
            return false;
        }

        $rezident->cislo_pokoje = $pokoj->cislo_pokoje;

        if (!$this->rezidentManager->getDao()->update($rezident)) {
            throw new \Exception("Nastala chyba při uložení");
        }

        return true;
    }

    /**
     * Přidání nové rezervace
     * @param Rezervace $rezervace
     * @return boolean True, pokud bylo uložení úspěšné
     */
    public function addRezervace(Rezervace $rezervace)
    {
        if (!$rezervace->validate()) {
            return false;
        }
        
        return $this->rezervaceManager->getDao()->add($rezervace);
    }

    /**
     * Přidání nového rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean True, pokud bylo uložení úspěšné
     */
    public function addRezident(Rezident $rezident)
    {        
        if (!$rezident->validate() || !$rezident->osoba->validate()) {
            return false;
        }        
        
        return $this->rezidentManager->getDao()->add($rezident);
    }

    /**
     * Pridání nového pokoje
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean True, pokod bylo uložení úspěšné
     */
    public function addPokoj(Pokoj $pokoj)
    {
        if (!$pokoj->validate()) {
            return false;
        }
        
        return $this->pokojManager->getDao()->add($pokoj);
    }

    /**
     * Odstranění rezervace
     * @param Rezervace $rezervace Rezervace ke smazání
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deleteRezervace(Rezervace $rezervace)
    {
        return $this->rezervaceManager->getDao()->remove($rezervace);
    }

    /**
     * Odstranění rezidenta
     * @param Rezident $rezident Rezident k odstranění
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deleteRezident(Rezident $rezident)
    {
        return $this->rezidentManager->getDao()->remove($rezident);
    }

    /**
     * Odstranění pokoje
     * @param Pokoj $pokoj Pokoj k odstranění
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deletePokoj(Pokoj $pokoj)
    {
        return $this->pokojManager->getDao()->remove($pokoj);
    }

    /**
     * Vrátí všechny pokoje
     * @return ArrayCollection<Pokoj> Všechny pokoje
     */
    public function getAllPokoj()
    {
        return $this->pokojManager->getDao()->getAll();
    }

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezident> Všichni rezidenti
     */
    public function getAllRezident()
    {
        return $this->rezidentManager->getDao()->getAll();
    }

    /**
     * Vrátí všechny rezervace
     * @return ArrayCollection<Rezervace> Všechny rezervace
     */
    public function getAllRezervace()
    {
        return $this->rezervaceManager->getDao()->getAll();
    }

    /**
     * Vrátí všechny volné pokoje
     * @return ArrayCollection<Pokoj> Všechny volné pokoje
     */
    public function getFreePokoj()
    {
        return $this->pokojManager->getDao()->getFree();
    }

    /**
     * Vrátí jeden pokoj podle ID (čísla pokoje)
     * @param integer $id Číslo pokoje
     * @return Pokoj|null Nalezený pokoj nebo null;
     */
    public function getOneByIdPokoj($id)
    {
        return $this->pokojManager->getDao()->getOneById($id);
    }

    /**
     * Vrátí jednoho rezidenta podle ID
     * @param integer $id ID rezidenta
     * @return Rezident|null Nalezený rezident nebo null
     */
    public function getOneByIdRezident($id)
    {
        return $this->rezidentManager->getDao()->getOneById($id);
    }

    /**
     * Vrátí jednu rezervaci podle ID
     * @param integer $id ID rezervace
     * @return Rezervace|null Nalezená rezervace nebo null
     */
    public function getOneByIdRezervace($id)
    {
        return $this->rezervaceManager->getDao()->getOneById($id);
    }

    /**
     * Nastavení pokoj manageru (DI)
     * @param pokojManager
     */
    public function setPokojManager(PokojManager $pokojManager)
    {
        $this->pokojManager = $pokojManager;
    }

    /**
     * Nastavení rezervace manageru (DI)
     * @param RezervaceManager $rezervaceManager
     */
    public function setRezervaceManager(RezervaceManager $rezervaceManager)
    {
        $this->rezervaceManager = $rezervaceManager;
    }

    /**
     * Nastavení rezident manageru (DI)
     * @param RezidentManager $rezidentManager
     */
    public function setRezidentManager(RezidentManager $rezidentManager)
    {
        $this->rezidentManager = $rezidentManager;
    }
    
    /**
     * Nastavení osoba manageru (DI)
     * @param OsobaManager $osobaManager
     */
    public function setOsobaManager(OsobaManager $osobaManager)
    {
        $this->osobaManager = $osobaManager;
    }

    /**
     * Uloží upravený pokoj
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean True, pokud byl pokoj uložen
     */
    public function updatePokoj(Pokoj $pokoj)
    {
        if (!$pokoj->validate()) {
            return false;
        }
        
        $actualCount = $this->pokojManager->getDao()->getCountOfRezident($pokoj->cislo_pokoje);
        
        if ($pokoj->kapacita < $actualCount) {
            return false;
        }
        
        return $this->pokojManager->getDao()->update($pokoj);
    }

    /**
     * Uloží upraveného rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean True, pokud byl rezident uložen
     */
    public function updateRezident(Rezident $rezident)
    {        
        if (!$rezident->validate() || !$rezident->osoba->validate()) {
            return false;
        } 
        
        return $this->rezidentManager->getDao()->update($rezident);
    }

    /**
     * Uloží upravenou rezervaci
     * @param Rezervace $rezervace Rezervace k uložení
     * @return boolean True, pokud byla rezervace uložen
     */
    public function updateRezervace(Rezervace $rezervace)
    {
        if (!$rezervace->validate()) {
            return false;
        }
        
        return $this->rezervaceManager->getDao()->update($rezervace);
    }

}
