<?php

namespace common\services\operations;

use common\models\entity\Aktivita;
use common\models\entity\Rezident;

/**
 * Operations pro aktivitu (vedoucího aktivity)
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:15:06
 */
interface IAktivityOperations
{

    /**
     * Přidání nové aktivity.
     * @param Aktivita $aktivita Aktivita k uložení
     * @return boolean True, pokud byl záznam uložen
     */
    public function addAktivita(Aktivita $aktivita);

    /**
     * Odstraní aktivitu
     * @param Aktivita $aktivita Aktivita k odstranění
     * @return boolean True, pokud byl záznam odstraněn
     */
    public function deleteAktivita(Aktivita $aktivita);

    /**
     * Vrátí všechny aktivity
     * @return ArrayCollection<Aktivita> Všechny aktivity
     */
    public function getAllAktivita();

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezindet> Všichni rezidenti
     */
    public function getAllRezident();

    /**
     * Vrátí jednu aktivitu podle ID
     * @param integer $id ID aktivity
     * @return Aktivita|null Nalezená aktivita nebo null;
     */
    public function getOneByIdAktivita($id);

    /**
     * Vrátí jednodo rezidenta podle jeho ID
     * @param integer $id ID rezidenta
     * @return Rezident|null Nalezený rezident nebo null;
     */
    public function getOneByIdRezident(integer $id);

    /**
     * Přihlásí rezidenta na danou aktivitu
     * @param Rezident $rezident Rezident který se hlásí na danou aktivitu
     * @param Aktivita $aktivita Aktivita, na kterou se rezident hlásí
     */
    public function loginToActivity(Rezident $rezident, Aktivita $aktivita);

    /**
     * Uloží upravenou aktivitu
     * @param Aktivita $aktivita Aktivita k uložení
     * @return boolean True, pokud byla aktivita uložena
     */
    public function updateAktivita(Aktivita $aktivita);
}
