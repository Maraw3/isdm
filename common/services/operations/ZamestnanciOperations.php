<?php

namespace common\services\operations;

/**
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:14:07
 */
class ZamestnanciOperations implements IZamestanciOperations
{

	protected $authManager;
	protected $osobaManager;
	public $m_OsobaManager;

	function __construct()
	{
	}

	function __destruct()
	{
	}



	/**
	 * 
	 * @param osoba
	 */
	public function addOsoba(Osoba $osoba)
	{
	}

	/**
	 * 
	 * @param osoba
	 * @param role
	 */
	public function assignmentRole(Osoba $osoba, Role $role)
	{
	}

	public function getAllOsoba()
	{
	}

	public function getAllRole()
	{
	}

	/**
	 * 
	 * @param id
	 */
	public function getOneByIdOsoba(int $id)
	{
	}

	/**
	 * 
	 * @param id
	 */
	public function getOneByIdRole(int $id)
	{
	}

	public function init()
	{
	}

	/**
	 * 
	 * @param osoba
	 */
	public function removeOsoba(Osoba $osoba)
	{
	}

	/**
	 * 
	 * @param manager
	 */
	public function setOsobaManager(OsobaManager $manager)
	{
	}

	/**
	 * 
	 * @param osoba
	 */
	public function updateOsoba(Osoba $osoba)
	{
	}

}
?>