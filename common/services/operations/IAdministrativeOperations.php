<?php

namespace common\services\operations;

use common\models\entity\Rezident;
use common\models\entity\Pokoj;
use common\models\entity\Rezervace;

/**
 * Interface pro Operations pro administrativního pracovníka
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:16:10
 */
interface IAdministrativeOperations
{

    /**
     * Ubytuje rezidenta na daný pokoj, pokud je to možné (na pokoji je místo)
     * @param Rezident $rezident Rezident k ubytování
     * @param Pokoj $pokoj Pokoj do kterého se má rezident nastěhovat
     * @return boolean Vratí true, pokud bylo ubytování úspěšné. False pokud je pokoj plný
     */
    public function accomodateRezident(Rezident $rezident, Pokoj $pokoj);

    /**
     * Přidání nové rezervace
     * @param Rezervace $rezervace Rezervace k uložení
     * @return boolean True, pokud bylo uložení úspěšné
     */
    public function addRezervace(Rezervace $rezervace);

    /**
     * Přidání nového rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean True, pokud bylo uložení úspěšné
     */
    public function addRezident(Rezident $rezident);

    /**
     * Pridání nového pokoje
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean True, pokod bylo uložení úspěšné
     */
    public function addPokoj(Pokoj $pokoj);

    /**
     * Odstranění rezervace
     * @param Rezervace $rezervace Rezervace ke smazání
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deleteRezervace(Rezervace $rezervace);

    /**
     * Odstranění rezidenta
     * @param Rezident $rezident Rezident k odstranění
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deleteRezident(Rezident $rezident);

    /**
     * Odstranění pokoje
     * @param Pokoj $pokoj Pokoj k odstranění
     * @return boolean True, pokud bylo odstranění úspěšné
     */
    public function deletePokoj(Pokoj $pokoj);

    /**
     * Vrátí všechny pokoje
     * @return ArrayCollection<Pokoj> Všechny pokoje
     */
    public function getAllPokoj();

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezident> Všichni rezidenti
     */
    public function getAllRezident();

    /**
     * Vrátí všechny rezervace
     * @return ArrayCollection<Rezervace> Všechny rezervace
     */
    public function getAllRezervace();

    /**
     * Vrátí všechny volné pokoje
     * @return ArrayCollection<Pokoj> Všechny volné pokoje
     */
    public function getFreePokoj();

    /**
     * Vrátí jeden pokoj podle ID (čísla pokoje)
     * @param integer $id Číslo pokoje
     * @return Pokoj|null Nalezený pokoj nebo null;
     */
    public function getOneByIdPokoj($id);

    /**
     * Vrátí jednoho rezidenta podle ID
     * @param integer $id ID rezidenta
     * @return Rezident|null Nalezený rezident nebo null
     */
    public function getOneByIdRezident($id);

    /**
     * Vrátí jednu rezervaci podle ID
     * @param integer $id ID rezervace
     * @return Rezervace|null Nalezená rezervace nebo null
     */
    public function getOneByIdRezervace($id);

    /**
     * Uloží upravený pokoj
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean True, pokud byl pokoj uložen
     */
    public function updatePokoj(Pokoj $pokoj);

    /**
     * Uloží upravenou rezervaci
     * @param Rezervace $rezervace Rezervace k uložení
     * @return boolean True, pokud byla rezervace uložen
     */
    public function updateRezervace(Rezervace $rezervace);

    /**
     * Uloží upraveného rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean True, pokud byl rezident uložen
     */
    public function updateRezident(Rezident $rezident);
}
