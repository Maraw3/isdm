<?php
namespace common\services\operations;

/**
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:15:32
 */
interface IZamestanciOperations
{

    /**
     * 
     * @param osoba
     */
    public function addOsoba(Osoba $osoba);

    /**
     * 
     * @param osoba
     * @param role
     */
    public function assignmentRole(Osoba $osoba, Role $role);

    public function getAllOsoba();

    public function getAllRole();

    /**
     * 
     * @param id
     */
    public function getOneByIdOsoba(int $id);

    /**
     * 
     * @param id
     */
    public function getOneByIdRole(int $id);

    /**
     * 
     * @param osoba
     */
    public function removeOsoba(Osoba $osoba);

    /**
     * 
     * @param osoba
     */
    public function updateOsoba(Osoba $osoba);
}

?>