<?php

return [
    'Update' => 'Upravit',
    'update' => 'upravit',
    'Create' => 'Vytvořit',
    'Delete' => 'Smazat',
    'Are you sure you want to delete this item?' => 'Opravdu chcete smazat tuto položku?',
];