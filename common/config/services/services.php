<?php

use mougrim\yii2ContainerConfigurator\ContainerConfigurator;

/* includes */
$managers = require __DIR__ . '/managers.php';
$operations = require __DIR__ . '/operations.php';
$daos = require __DIR__ . '/dao.php';
$forms = require __DIR__ . '/forms.php';

/* other services */
$service = [
    'front.controllers.rezident' => [
        'class' => 'frontend\controllers\RezidentController',
        'call' => [
            'setAdministrativeOperations' => [
                [
                    'id' => 'operations.administrative',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE
                ]
            ]
        ]
    ],
    'front.controllers.pokoj' => [
        'class' => 'frontend\controllers\PokojController',
        'call' => [
            'setAdministrativeOperations' => [
                [
                    'id' => 'operations.administrative',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE
                ]
            ]
        ]
    ]
];

return array_merge($managers, $operations, $daos, $forms, $service);
