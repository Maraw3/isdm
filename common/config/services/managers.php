<?php

use mougrim\yii2ContainerConfigurator\ContainerConfigurator;

return [
    'managers.aktivita' => [
        'class' => 'common\services\managers\AktivitaManager',
        'call' => [
            'setDao' => [
                [
                    'id' => 'models.dao.aktivita',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ]
        ]
    ],
    'managers.osoba' => [
        'class' => 'common\services\managers\OsobaManager',
        'call' => [
            'setDao' => [
                [
                    'id' => 'models.dao.osoba',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ]
        ]
    ],
    'managers.pokoj' => [
        'class' => 'common\services\managers\PokojManager',
        'call' => [
            'setDao' => [
                [
                    'id' => 'models.dao.pokoj',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ]
        ]
    ],
    'managers.rezervace' => [
        'class' => 'common\services\managers\RezervaceManager',
        'call' => [
            'setDao' => [
                [
                    'id' => 'models.dao.rezervace',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ]
        ]
    ],
    'managers.rezident' => [
        'class' => 'common\services\managers\RezidentManager',
        'call' => [
            'setDao' => [
                [
                    'id' => 'models.dao.rezident',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ]
        ]
    ],
];
