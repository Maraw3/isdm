<?php

use mougrim\yii2ContainerConfigurator\ContainerConfigurator;

return [
    'operations.administrative' => [
        'class' => 'common\services\operations\AdministrativeOperations',
        'call' => [
            'setRezidentManager' => [
                [
                    'id' => 'managers.rezident',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ],
            'setPokojManager' => [
                [
                    'id' => 'managers.pokoj',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ],
            'setRezervaceManager' => [
                [
                    'id' => 'managers.rezervace',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ],
            'setOsobaManager' => [
                [
                    'id' => 'managers.osoba',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE,
                ]
            ],
        ]
    ],
];
