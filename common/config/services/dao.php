<?php

use mougrim\yii2ContainerConfigurator\ContainerConfigurator;

return [
    'models.dao.aktivita' => [
        'class' => 'common\models\dao\AktivitaDAO'
    ],
    'models.dao.lek' => [
        'class' => 'common\models\dao\LekDAO'
    ],
    'models.dao.nejblizsiOsoba' => [
        'class' => 'common\models\dao\NeblizsiOsobaDAO'
    ],
    'models.dao.osetreni' => [
        'class' => 'common\models\dao\OsetreniDAO'
    ],
    'models.dao.osoba' => [
        'class' => 'common\models\dao\OsobaDAO'
    ],
    'models.dao.pokoj' => [
        'class' => 'common\models\dao\PokojDAO'
    ],
    'models.dao.rezervace' => [
        'class' => 'common\models\dao\RezervaceDAO'
    ],
    'models.dao.rezident' => [
        'class' => 'common\models\dao\RezidentDAO'
    ]
];
