<?php

use mougrim\yii2ContainerConfigurator\ContainerConfigurator;

return [
    'models.forms.accomodate' => [
        'class' => 'common\models\forms\AccomodateForm',
        'call' => [
            'setAdministrativeOperations' => [
                [
                    'id' => 'operations.administrative',
                    'type' => ContainerConfigurator::ARGUMENT_TYPE_REFERENCE
                ]
            ]
        ]
    ],
];
