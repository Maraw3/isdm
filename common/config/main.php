<?php
use mougrim\yii2ContainerConfigurator\ContainerConfigurator;
use yii\di\Container;

Yii::$container->set(
    'containerConfigurator',
    function (Container $container) {
        $containerConfigurator = new ContainerConfigurator($container);
        $containerConfigurator->configure(require __DIR__ . '/services/services.php'); 
        return $containerConfigurator;
    }
);

return [
    'bootstrap' => ['containerConfigurator'],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'containerConfigurator' => 'containerConfigurator',
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i',
            'timeFormat' => 'php:H:i',
        ]
    ],
    /* Set controller as service */
    'controllerMap' => [
        'rezident' => 'front.controllers.rezident',
    ],
];
