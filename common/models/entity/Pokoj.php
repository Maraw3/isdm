<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Pokoj".
 *
 * @property integer $cislo_pokoje
 * @property integer $kapacita
 *
 * @property Rezident[] $rezidents
 */
class Pokoj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pokoj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cislo_pokoje', 'kapacita'], 'required'],
            [['cislo_pokoje', 'kapacita'], 'integer'],
            [['kapacita'], 'integer', 'min' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cislo_pokoje' => Yii::t('app', 'Cislo Pokoje'),
            'kapacita' => Yii::t('app', 'Kapacita'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezidents()
    {
        return $this->hasMany(Rezident::className(), ['cislo_pokoje' => 'cislo_pokoje']);
    }
}
