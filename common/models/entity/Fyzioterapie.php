<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Fyzioterapie".
 *
 * @property string $datum
 * @property integer $id
 * @property string $nazev
 * @property string $popis
 * @property integer $nemocice_id
 * @property integer $zdravotni_potiz_id
 *
 * @property ZdravotniPotiz $zdravotniPotiz
 * @property Nemocnice $nemocice
 */
class Fyzioterapie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fyzioterapie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datum', 'nazev'], 'required'],
            [['datum'], 'safe'],
            [['nemocice_id', 'zdravotni_potiz_id'], 'integer'],
            [['nazev'], 'string', 'max' => 50],
            [['popis'], 'string', 'max' => 2048]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'datum' => Yii::t('app', 'Datum'),
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'popis' => Yii::t('app', 'Popis'),
            'nemocice_id' => Yii::t('app', 'Nemocice ID'),
            'zdravotni_potiz_id' => Yii::t('app', 'Zdravotni Potiz ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZdravotniPotiz()
    {
        return $this->hasOne(ZdravotniPotiz::className(), ['id' => 'zdravotni_potiz_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNemocice()
    {
        return $this->hasOne(Nemocnice::className(), ['id' => 'nemocice_id']);
    }
}
