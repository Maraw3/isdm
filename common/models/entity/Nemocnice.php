<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Nemocnice".
 *
 * @property integer $id
 * @property string $nazev
 * @property integer $adresa_id
 *
 * @property Fyzioterapie[] $fyzioterapies
 * @property Adresa $adresa
 * @property Operace[] $operaces
 */
class Nemocnice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nemocnice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev', 'adresa_id'], 'required'],
            [['adresa_id'], 'integer'],
            [['nazev'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'adresa_id' => Yii::t('app', 'Adresa ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFyzioterapies()
    {
        return $this->hasMany(Fyzioterapie::className(), ['nemocice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresa()
    {
        return $this->hasOne(Adresa::className(), ['id' => 'adresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperaces()
    {
        return $this->hasMany(Operace::className(), ['nemocnice_id' => 'id']);
    }
}
