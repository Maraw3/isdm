<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Role".
 *
 * @property integer $id
 * @property string $nazev
 * @property string $popis
 *
 * @property OsobaMaRoli[] $osobaMaRolis
 * @property Osoba[] $osobas
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev'], 'required'],
            [['nazev'], 'string', 'max' => 50],
            [['popis'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'popis' => Yii::t('app', 'Popis'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsobaMaRolis()
    {
        return $this->hasMany(OsobaMaRoli::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsobas()
    {
        return $this->hasMany(Osoba::className(), ['id' => 'osoba_id'])->viaTable('OsobaMaRoli', ['role_id' => 'id']);
    }
}
