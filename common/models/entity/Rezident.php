<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Rezident".
 *
 * @property string $zaplaceno_do
 * @property string $nastup
 * @property integer $cislo_pokoje
 * @property integer $osoba_id
 *
 * @property Aktivita[] $aktivitas
 * @property Operace[] $operaces
 * @property Pokoj $cisloPokoje
 * @property ZdravotniPotiz[] $zdravotniPotizs
 */
class Rezident extends \yii\db\ActiveRecord
{

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_ACCOMODATE = 'accomodate';

    /**
     * @var Osoba
     */
    private $osoba;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rezident';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zaplaceno_do', 'nastup'], 'required'],
            [['zaplaceno_do', 'nastup'], 'date', 'format' => 'php:Y-m-d'],
            [['cislo_pokoje', 'osoba_id'], 'integer'],
            [['osoba_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['zaplaceno_do', 'nastup', 'osoba_id', 'cislo_pokoje'],
            self::SCENARIO_UPDATE => ['zaplaceno_do', 'nastup', 'osoba_id', 'cislo_pokoje'],
            self::SCENARIO_ACCOMODATE => ['cislo_pokoje'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'common\components\behaviours\DateFormatBehaviour',
                'attributes' => [
                    'zaplaceno_do',
                    'nastup',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zaplaceno_do' => Yii::t('app', 'Zaplaceno Do'),
            'nastup' => Yii::t('app', 'Nastup'),
            'cislo_pokoje' => Yii::t('app', 'Cislo Pokoje'),
            'osoba_id' => Yii::t('app', 'Osoba ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNejblizsiOsoba()
    {
        return $this->hasOne(NejblizsiOsoba::className(), ['rezident_id' => 'osoba_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsoba()
    {
        if ($this->osoba == null) {
            $this->osoba = $this->hasOne(Osoba::className(), ['id' => 'osoba_id'])->one();
        }

        return $this->osoba;
    }

    public function setOsoba(Osoba $osoba)
    {
        if ($this->osoba == null) {
            $this->osoba = $osoba;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCisloPokoje()
    {
        return $this->hasOne(Pokoj::className(), ['cislo_pokoje' => 'cislo_pokoje']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZdravotniPotizs()
    {
        return $this->hasMany(ZdravotniPotiz::className(), ['rezident_id' => 'osoba_id']);
    }

    public function beforeSave($insert)
    {
        if (is_object($this->osoba)) {
            $this->osoba->save(false);
        }
        
        if ($this->osoba_id == null) {
            $this->osoba_id = $this->osoba->id;
        }

        if ($insert) {
            /* Yii2 RBAC for Rezident */
            \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(Osoba::ROLE_REZIDENT), $this->osoba_id);
        }

        return parent::beforeSave($insert);
    }

}
