<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Lek".
 *
 * @property integer $id
 * @property string $nazev
 * @property string $davkovani
 * @property integer $cena
 *
 * @property MaPredepsano[] $maPredepsanos
 * @property ZdravotniPotiz[] $potizs
 */
class Lek extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lek';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev', 'davkovani', 'cena'], 'required'],
            [['cena'], 'integer'],
            [['nazev'], 'string', 'max' => 50],
            [['davkovani'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'davkovani' => Yii::t('app', 'Davkovani'),
            'cena' => Yii::t('app', 'Cena'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaPredepsanos()
    {
        return $this->hasMany(MaPredepsano::className(), ['lek_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPotizs()
    {
        return $this->hasMany(ZdravotniPotiz::className(), ['id' => 'potiz_id'])->viaTable('maPredepsano', ['lek_id' => 'id']);
    }
}
