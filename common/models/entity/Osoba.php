<?php

namespace common\models\entity;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "Osoba".
 *
 * @property integer $id
 * @property string $jmeno
 * @property string $prijmeni
 * @property integer $rodneCislo
 * @property string $email
 * @property string $heslo_hash
 * @property integer $adresa_id
 *
 * @property Aktivita[] $aktivitas
 * @property NejblizsiOsoba[] $nejblizsiOsoby
 * @property Rezident[] $rezident
 * @property Adresa $adresa
 * @property OsobaMaRoli[] $osobaMaRole
 * @property Role[] $roles
 * @property Rezervace[] $rezervace
 * @property ZdravotniPotiz[] $zdravotniPotize
 */
class Osoba extends ActiveRecord implements IdentityInterface
{

    const ROLE_ADMIN = 'admin';
    const ROLE_SUPERVISOR = 'supervisor';
    const ROLE_REZIDENT = 'rezident';
    const ROLE_LEKAR = 'lekar';
    const ROLE_FYZIOTERAPEUT = 'fyzioterapeut';
    const ROLE_ADMINISTRATIVA = 'administrativa';
    const ROLE_OSETREVOTEL = 'osetrevatel';
    const ROLE_VEDOUCI_AKTIVITY = 'vedouci-aktivity';
    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'osoba';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jmeno', 'prijmeni', 'rodneCislo', 'email', 'heslo_hash'], 'required'],
            [['adresa_id', 'rodneCislo'], 'integer'],
            [['jmeno', 'prijmeni'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 80],
            [['email'], 'email'],
            [['rodneCislo'], 'string', 'length' => 9],
            [['heslo_hash'], 'string', 'max' => 256]
        ];
    }
    
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['jmeno', 'prijmeni', 'rodneCislo', 'email', 'password_hash'],
            self::SCENARIO_UPDATE => ['jmeno', 'prijmeni', 'rodneCislo', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jmeno' => Yii::t('app', 'Jmeno'),
            'prijmeni' => Yii::t('app', 'Prijmeni'),
            'rodneCislo' => Yii::t('app', 'Rodne Cislo'),
            'email' => Yii::t('app', 'Email'),
            'heslo_hash' => Yii::t('app', 'Heslo Hash'),
            'adresa_id' => Yii::t('app', 'Adresa ID'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->heslo_hash = \Yii::$app->security->generatePasswordHash($this->heslo_hash);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = static::findOne(['id' => $id]);
        return $user;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username)
    {
        return static::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->heslo_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->heslo_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAktivitas()
    {
        return $this->hasMany(Aktivita::className(), ['osoba_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNejblizsiOsobas()
    {
        return $this->hasMany(NejblizsiOsoba::className(), ['osoba_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresa()
    {
        return $this->hasOne(Adresa::className(), ['id' => 'adresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsobaMaRolis()
    {
        return $this->hasMany(OsobaMaRoli::className(), ['osoba_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Role::className(), ['id' => 'role_id'])->viaTable('OsobaMaRoli', ['osoba_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezervaces()
    {
        return $this->hasMany(Rezervace::className(), ['id_osoba' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezidents0()
    {
        return $this->hasMany(Rezident::className(), ['osoba_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZdravotniPotizs()
    {
        return $this->hasMany(ZdravotniPotiz::className(), ['doktor_id' => 'id']);
    }

}
