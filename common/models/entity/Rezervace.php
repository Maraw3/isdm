<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Rezervace".
 *
 * @property string $poznaky
 * @property integer $id
 * @property integer $id_osoba
 * @property integer $stav_id
 *
 * @property Osoba $idOsoba
 * @property StavRezervace $stav
 */
class Rezervace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rezervace';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_osoba', 'stav_id'], 'integer'],
            [['poznaky'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'poznaky' => Yii::t('app', 'Poznaky'),
            'id' => Yii::t('app', 'ID'),
            'id_osoba' => Yii::t('app', 'Id Osoba'),
            'stav_id' => Yii::t('app', 'Stav ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOsoba()
    {
        return $this->hasOne(Osoba::className(), ['id' => 'id_osoba']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStav()
    {
        return $this->hasOne(StavRezervace::className(), ['id' => 'stav_id']);
    }
}
