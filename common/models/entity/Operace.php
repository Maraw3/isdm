<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Operace".
 *
 * @property integer $id
 * @property string $nazev
 * @property string $diagnoza
 * @property string $datum
 * @property integer $nemocnice_id
 * @property integer $rezident_id
 *
 * @property Rezident $rezident
 * @property Nemocnice $nemocnice
 */
class Operace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operace';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev', 'diagnoza', 'datum', 'nemocnice_id', 'rezident_id'], 'required'],
            [['datum'], 'safe'],
            [['nemocnice_id', 'rezident_id'], 'integer'],
            [['nazev'], 'string', 'max' => 50],
            [['diagnoza'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'diagnoza' => Yii::t('app', 'Diagnoza'),
            'datum' => Yii::t('app', 'Datum'),
            'nemocnice_id' => Yii::t('app', 'Nemocnice ID'),
            'rezident_id' => Yii::t('app', 'Rezident ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezident()
    {
        return $this->hasOne(Rezident::className(), ['osoba_id' => 'rezident_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNemocnice()
    {
        return $this->hasOne(Nemocnice::className(), ['id' => 'nemocnice_id']);
    }
}
