<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "adresa".
 *
 * @property integer $id
 * @property string $mesto
 * @property integer $orientacni_cislo
 * @property integer $popisne_cislo
 * @property integer $psc
 * @property string $ulice
 *
 * @property Nemocnice[] $nemocnices
 * @property Osoba[] $osobas
 */
class Adresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mesto', 'orientacni_cislo', 'popisne_cislo', 'psc', 'ulice'], 'required'],
            [['orientacni_cislo', 'popisne_cislo', 'psc'], 'integer'],
            [['mesto', 'ulice'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mesto' => Yii::t('app', 'Mesto'),
            'orientacni_cislo' => Yii::t('app', 'Orientacni Cislo'),
            'popisne_cislo' => Yii::t('app', 'Popisne Cislo'),
            'psc' => Yii::t('app', 'Psc'),
            'ulice' => Yii::t('app', 'Ulice'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNemocnices()
    {
        return $this->hasMany(Nemocnice::className(), ['adresa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsobas()
    {
        return $this->hasMany(Osoba::className(), ['adresa_id' => 'id']);
    }
}
