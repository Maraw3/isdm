<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "NejblizsiOsoba".
 *
 * @property string $vztah
 * @property integer $osoba_id
 * @property integer $rezident_id
 *
 * @property Rezident $rezident
 * @property Osoba $osoba
 */
class NejblizsiOsoba extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nejblizsiosoba';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vztah', 'osoba_id', 'rezident_id'], 'required'],
            [['osoba_id', 'rezident_id'], 'integer'],
            [['vztah'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vztah' => Yii::t('app', 'Vztah'),
            'osoba_id' => Yii::t('app', 'Osoba ID'),
            'rezident_id' => Yii::t('app', 'Rezident ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezident()
    {
        return $this->hasOne(Rezident::className(), ['osoba_id' => 'rezident_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsoba()
    {
        return $this->hasOne(Osoba::className(), ['id' => 'osoba_id']);
    }
}
