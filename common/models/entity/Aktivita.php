<?php

namespace common\models\entity;

use Yii;

/**
 * This is the model class for table "Aktivita".
 *
 * @property integer $id
 * @property string $nazev
 * @property string $popis
 * @property integer $cena
 * @property integer $osoba_id
 *
 * @property Osoba $osoba
 * @property AktivitaPrihlaseni[] $aktivitaPrihlasenis
 * @property Rezident[] $rezidents
 */
class Aktivita extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aktivita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazev', 'popis', 'cena'], 'required'],
            [['cena', 'osoba_id'], 'integer'],
            [['nazev'], 'string', 'max' => 50],
            [['popis'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nazev' => Yii::t('app', 'Nazev'),
            'popis' => Yii::t('app', 'Popis'),
            'cena' => Yii::t('app', 'Cena'),
            'osoba_id' => Yii::t('app', 'Osoba ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsoba()
    {
        return $this->hasOne(Osoba::className(), ['id' => 'osoba_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAktivitaPrihlasenis()
    {
        return $this->hasMany(AktivitaPrihlaseni::className(), ['aktivita_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRezidents()
    {
        return $this->hasMany(Rezident::className(), ['id' => 'rezident_id'])->viaTable('AktivitaPrihlaseni', ['aktivita_id' => 'id']);
    }
}
