<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\services\operations\IAdministrativeOperations;
use common\models\entity\Rezident;

/**
 * Login form
 * 
 */
class AccomodateForm extends Model
{

    /**
     * Operace pro administrativu
     * @var IAdministrativeOperations
     */
    private $administrativeOperations;

    /**
     * ID osoby
     * @var integer
     */
    public $rezident;

    /**
     * Čílso pokoje
     * @var integer
     */
    public $pokoj;

    /**
     * Pravidla pro validaci
     * @return array
     */
    public function rules()
    {
        return [
            [['rezident', 'pokoj'], 'required'],
            [['rezident', 'pokoj'], 'integer'],
        ];
    }

    /**
     * Proveď přiřazení na pokoj
     * @return boolean True, pokud byl rezident přidělen na daný pokoj
     */
    public function accomodate()
    {
        $rezident = $this->administrativeOperations->getOneByIdRezident($this->rezident);
        $rezident->scenario = Rezident::SCENARIO_ACCOMODATE;
        $pokoj = $this->administrativeOperations->getOneByIdPokoj($this->pokoj);

        return $this->administrativeOperations->accomodateRezident($rezident, $pokoj);
    }

    /**
     * Vrátí všechny rezidenty k formuláři
     * @return array Namapované array pro select
     */
    public function getRezidents()
    {
        return ArrayHelper::map($this->administrativeOperations->getAllRezident()->toArray(), 'osoba_id', function($model, $defaultValue) {
            return $model->osoba->jmeno . ' ' . $model->osoba->prijmeni . ' (Aktualní pokoj: ' . $model->cislo_pokoje . ')';
        });
    }

    /**
     * Vrátí všechny pokoje k formuláři
     * @return array Namapované array pro select
     */
    public function getRooms()
    {
        return ArrayHelper::map($this->administrativeOperations->getFreePokoj()->toArray(), 'cislo_pokoje', 'cislo_pokoje');
    }

    /**
     * Nastaví administrative operations (DI)
     * @param IAdministrativeOperations $ao
     */
    public function setAdministrativeOperations(IAdministrativeOperations $ao)
    {
        $this->administrativeOperations = $ao;
    }

}
