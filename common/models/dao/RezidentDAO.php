<?php

namespace common\models\dao;

use common\models\entity\Rezident;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Rezident
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:04
 */
class RezidentDAO implements IRezidentDAO
{

    /**
     * Uloží nového rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Rezident $rezident)
    {
        return $rezident->save(false);
    }

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezident> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(Rezident::find()->all());
    }

    /**
     * Vrátí rezidenta podle jeho ID (osoba_id).
     * @param integer $id ID hledaného rezidenty
     * @return Rezident Vrací nalezeného rezidenta, nebo null
     */
    public function getOneById($id)
    {
        return Rezident::findOne(['osoba_id' => $id]);
    }

    /**
     * Vymaže rezidenta
     * @param Rezident $rezident Rezident ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Rezident $rezident)
    {
        return $rezident->delete();
    }

    /**
     * Uloží upraveného rezidenta
     * @param Rezident $rezident Upravená rezidenta k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Rezident $rezident)
    {
        return $rezident->save(false);
    }

}
