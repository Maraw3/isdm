<?php

namespace common\models\dao;

use common\models\entity\Lek;

/**
 * Interface pro Data access object pro Lek

 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:15
 */
interface ILekDAO
{

    /**
     * Uloží nový lék
     * @param Lek $lek Lék k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Lek $lek);

    /**
     * Vrátí všechny léky
     * @return ArrayCollection<Lek> Vrací všechny záznamy
     */
    public function getAll();

    /**
     * Vrátí lék podle jejího ID.
     * @param integer $id ID hledaného léku
     * @return ActiveRecord Vrací nalezený lék, nebo null
     */
    public function getOneById(int $id);

    /**
     * Vymaže lek
     * @param Lek $lek Lék ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Lek $lek);

    /**
     * Uloží upravený lek
     * @param Lek $lek Upravený lék k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Lek $lek);
}
