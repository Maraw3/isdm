<?php

namespace common\models\dao;

use common\models\entity\Pokoj;

/**
 * Interface pro Data access object pro Pokoj
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:06
 */
interface IPokojDAO
{

    /**
     * Uloží nový pokoj
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Pokoj $pokoj);

    /**
     * Vrátí všechny pokoje
     * @return ArrayCollection<Pokoj> Vrací všechny záznamy
     */
    public function getAll();

    /**
     * Najde pokoje podle statusu
     * @param string $status
     * @return Pokoj Vrací nalezené pokoje
     */
    public function getByState($status);

    /**
     * Počet rezidentů na pokoji
     * @param integer $cislo Číslo pokoje
     * @return integer Počet ubytovaných rezidentů na pokoji
     */
    public function getCountOfRezident($cislo);

    /**
     * Najde všechny pokoje, které nejsou plné
     * @return ArrayCollection<Pokoj> Vrací všechny nalezené pokoje
     */
    public function getFree();

    /**
     * Vrátí pokoj podle čísla
     * @param integer $cislo Číslo hledaného pokoje
     * @return Pokoj Vrací nalezený osoba, nebo null
     */
    public function getOneById($cislo);

    /**
     * Vymaže pokoj
     * @param Pokoj $pokoj Pokoj ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Pokoj $pokoj);

    /**
     * Uloží upravený pokoj
     * @param Pokoj $pokoj Upravený pokoj k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Pokoj $pokoj);
}
