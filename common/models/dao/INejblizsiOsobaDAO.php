<?php

namespace common\models\dao;

use common\models\entity\NejblizsiOsoba;

/**
 * Interface pro Data access object pro NejblizsiOsoba
 * 
 * @author bruanmar
 * @version 1.0
 * @created 15-XII-2015 16:11:13
 */
interface INejblizsiOsobaDAO
{

    /**
     * Uloží novou nejbližší osobu
     * @param NejblizsiOsoba $nejblizsiOsoba Nejbližší osoba k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(NejblizsiOsoba $nejblizsiOsoba);

    /**
     * Vrátí všechny nejbližší osoby
     * @return ArrayCollection<NejblizsiOsoba> Vrací všechny záznamy
     */
    public function getAll();

    /**
     * Vrátí nejbližší osobu podle jejího ID.
     * @param integer $id ID hledané nejbližší osoby
     * @return ActiveRecord Vrací nalezený nejbližší osoba, nebo null
     */
    public function getOneById($id);

    /**
     * Vymaže nejbližší osobu
     * @param NejblizsiOsoba $nejblizsiOsoba Nejbližší osoba ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(NejblizsiOsoba $nejblizsiOsoba);

    /**
     * Uloží upravenou nejbližší osobu
     * @param NejblizsiOsoba $nejblizsiOsoba Upravená nejbližší osoba k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(NejblizsiOsoba $nejblizsiOsoba);
}
