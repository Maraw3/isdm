<?php

namespace common\models\dao;

use common\models\entity\NejblizsiOsoba;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro NejblizsiOsoba
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:14
 */
class NejblizsiOsobaDAO implements INejblizsiOsobaDAO
{

    /**
     * Uloží novou nejbližší osobu
     * @param NejblizsiOsoba $nejblizsiOsoba Nejbližší osoba k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(NejblizsiOsoba $nejblizsiOsoba)
    {
        return $nejblizsiOsoba->save(false);
    }

    /**
     * Vrátí všechny nejblizsiOsobay
     * @return ArrayCollection<NejblizsiOsoba> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(NejblizsiOsoba::find()->all());
    }

    /**
     * Vrátí nejblizsiOsobau podle jejího ID.
     * @param integer $id ID hledané nejbližší osoby
     * @return NejblizsiOsoba Vrací nalezený nejbližší osoba, nebo null
     */
    public function getOneById($id)
    {
        return NejblizsiOsoba::findOne(['id' => $id]);
    }

    /**
     * Vymaže nejblizsiOsoba
     * @param NejblizsiOsoba $nejblizsiOsoba Nejbližší osoba ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(NejblizsiOsoba $nejblizsiOsoba)
    {
        return $nejblizsiOsoba->delete();
    }

    /**
     * Uloží upravenou nejblizsiOsoba
     * @param NejblizsiOsoba $nejblizsiOsoba Upravená nejbližší osoba k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(NejblizsiOsoba $nejblizsiOsoba)
    {
        return $nejblizsiOsoba->save(false);
    }

}
