<?php

namespace common\models\dao;

use common\models\entity\Pokoj;
use common\models\entity\Rezident;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Pokoj
 * 
 * @author notebook
 * @version 1.0
 * @created 15-XII-2015 16:11:05
 */
class PokojDAO implements IPokojDAO
{

    /**
     * Uloží nový pokoj
     * @param Pokoj $pokoj Pokoj k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Pokoj $pokoj)
    {
        return $pokoj->save(false);
    }

    /**
     * Vrátí všechny pokoje
     * @return ArrayCollection<Pokoj> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(Pokoj::find()->all());
    }

    /**
     * Najde pokoje podle statusu
     * @param string $status
     * @return Pokoj Vrací nalezené pokoje
     */
    public function getByState($status)
    {
        return new ArrayCollection(Pokoj::findAll([
                ])->all());
    }

    /**
     * Najde všechny pokoje, které nejsou plné
     * @return ArrayCollection<Pokoj> Vrací všechny nalezené pokoje
     */
    public function getFree()
    {
        $q = Rezident::find()->select('COUNT(*)')
                        ->groupBy('cislo_pokoje')
                        ->createCommand()->getSql();

        return new ArrayCollection(Pokoj::find()->where([
            ">", "kapacita", $q
        ])->all());
    }
    
    /**
     * Počet rezidentů na pokoji
     * @param integer $cislo Číslo pokoje
     * @return integer Počet ubytovaných rezidentů na pokoji
     */
    public function getCountOfRezident($cislo)
    {
        return Rezident::find()
               ->where(['cislo_pokoje' => $cislo])
               ->count();
    }

    /**
     * Vrátí pokoj podle čísla
     * @param integer $cislo Číslo hledaného pokoje
     * @return Pokoj Vrací nalezený osoba, nebo null
     */
    public function getOneById($cislo)
    {
        return Pokoj::findOne([
            'cislo_pokoje' => $cislo,
        ]);
    }

    /**
     * Vymaže pokoj
     * @param Pokoj $pokoj Pokoj ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Pokoj $pokoj)
    {
        return $pokoj->delete();
    }

    /**
     * Uloží upravený pokoj
     * @param Pokoj $pokoj Upravený pokoj k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Pokoj $pokoj)
    {
        return $pokoj->save(false);
    }

}
