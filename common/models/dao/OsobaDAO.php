<?php

namespace common\models\dao;

use common\models\entity\Osoba;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Osoba
 *
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:18
 */
class OsobaDAO implements IOsobaDAO
{

    /**
     * Uloží novou osobu
     * @param Osoba $osoba Osoba k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Osoba $osoba)
    {
        return $osoba->save(false);
    }

    /**
     * Vrátí všechny osoby
     * @return ArrayCollection<Osoba> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(Osoba::find()->all());
    }

    /**
     * Najde osobu podle jejího emailu
     * @param string $email
     * @return Osoba Vrací nalezenou osobu nebo null
     */
    public function getByEmail($email)
    {
        return Osoba::findOne(['email' => $email]);
    }

    /**
     * Najde uživatele podle specifické role
     * @param integer $id
     * @return ArrayCollection<Osoba> Všechny osoby podle zvolené role
     */
    public function getByRole($id)
    {
        return new ArrayCollection(Osoba::find()->joinWith('OsobaMaRoli')
                        ->where(['OsobaMaRoli.role_id' => $id])
                        ->all());
    }

    /**
     * Vrátí osobau podle jejího ID.
     * @param integer $id ID hledané osoby
     * @return Osoba Vrací nalezený osoba, nebo null
     */
    public function getOneById($id)
    {
        return Osoba::findAll(['id' => $id]);
    }

    /**
     * Vymaže osoba
     * @param Osoba $osoba Osoba ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Osoba $osoba)
    {
        return $osoba->delete();
    }

    /**
     * Uloží upravenou osoba
     * @param Osoba $osoba Upravená osoba k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Osoba $osoba)
    {
        return $osoba->save(false);
    }

}
