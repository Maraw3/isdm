<?php

namespace common\models\dao;

use common\models\entity\Aktivita;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Aktivita
 * 
 * @author Bruanmar
 * @version 1.0
 * @created 15-XII-2015 16:11:10
 */
class AktivitaDAO implements IAktivitaDAO
{
    /**
     * Uloží novou aktivitu
     * @param Aktivita $aktivita
     * @return boolean Výsledek, zda bylo uložení úspěšné
     */
    public function add(Aktivita $aktivita)
    {
        return $aktivita->save(false);
    }

    /**
     * Vrátí všechny aktivity
     * @return ArrayCollection<Aktivita> Všechny aktivity
     */
    public function getAll()
    {
        return new ArrayCollection(Aktivita::find()->all());
    }

    /**
     * Vrátí aktivitu podle jejího ID.
     * @param integer $id
     * @return Aktivita Vrací nalezenou aktivitu, nebo null 
     */
    public function getOneById($id)
    {
        return Aktivita::findOne(['id' => $id]);
    }

    /**
     * Vymaže aktivitu
     * @param Aktivita $aktivita
     * @return boolean
     */
    public function remove(Aktivita $aktivita)
    {
        return $aktivita->delete();
    }

    /**
     * Uloží upravenou aktivitu
     * @param Aktivita $aktivita
     * @return boolean
     */
    public function update(Aktivita $aktivita)
    {
        return $aktivita->save(false);
    }

}