<?php

namespace common\models\dao;

use common\models\entity\Rezident;

interface IRezidentDao
{
    public function getAll();
    
    public function getOneById($id);    
    
    public function add(Rezident $rezident);
    
    public function remove(Rezident $rezident);
    
    public function update(Rezident $rezident);
}