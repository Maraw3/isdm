<?php

namespace common\models\dao;

use common\models\entity\Rezident;

/**
 * Interface pro Data access object pro Rezident
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:07
 */
interface IRezidentDAO
{

    /**
     * Uloží nového rezidenta
     * @param Rezident $rezident Rezident k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Rezident $rezident);

    /**
     * Vrátí všechny rezidenty
     * @return ArrayCollection<Rezident> Vrací všechny záznamy
     */
    public function getAll();

    /**
     * Vrátí rezidenta podle jeho ID (osoba_id).
     * @param integer $id ID hledaného rezidenty
     * @return Rezident Vrací nalezeného rezidenta, nebo null
     */
    public function getOneById($id);

    /**
     * Vymaže rezidenta
     * @param Rezident $rezident Rezident ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Rezident $rezident);

    /**
     * Uloží upraveného rezidenta
     * @param Rezident $rezident Upravená rezidenta k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Rezident $rezident);
}
