<?php

namespace common\models\dao;

use common\models\entity\Osoba;

/**
 * Interface pro Data access object pro Osoba
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:17
 */
interface IOsobaDAO
{

    /**
     * Uloží novou osobu
     * @param Osoba $osoba Osoba k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Osoba $osoba);

     /**
     * Vrátí všechny osoby
     * @return ArrayCollection<Osoba> Vrací všechny záznamy
     */
    public function getAll();

    /**
     * Najde osobu podle jejího emailu
     * @param string $email
     * @return Osoba Vrací nalezenou osobu nebo null
     */
    public function getByEmail($email);

    /**
     * Najde uživatele podle specifické role
     * @param integer $id
     * @return ArrayCollection<Osoba> Všechny osoby podle zvolené role
     */
    public function getByRole($id);

    /**
     * Vrátí osobu podle jejího ID.
     * @param integer $id ID hledané osoby
     * @return ActiveRecord Vrací nalezený osoba, nebo null
     */
    public function getOneById($id);

    /**
     * Vymaže osobu
     * @param Osoba $osoba Osoba ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Osoba $osoba);

    /**
     * Uloží upravenou osobu
     * @param Osoba $osoba Upravená osoba k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Osoba $osoba);
}
