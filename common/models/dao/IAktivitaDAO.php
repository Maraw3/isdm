<?php

namespace common\models\dao;

use common\models\entity\Aktivita;

/**
 * Interface pro Data access object pro Aktivita
 * 
 * @author Braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:10
 */
interface IAktivitaDAO
{

    /**
     * Uloží novou aktivitu
     * @param Aktivita $aktivita
     * @return boolean Výsledek, zda bylo uložení úspěšné
     */
    public function add(Aktivita $aktivita);

   /**
     * Vrátí všechny aktivity
     * @return ArrayCollection<Aktivita> Všechny aktivity
     */
    public function getAll();

    /**
     * Vrátí aktivitu podle jejího ID.
     * @param integer $id
     * @return Aktivita
     */
    public function getOneById($id);

    /**
     * Vymaže aktivitu
     * @param Aktivita $aktivita
     * @return boolean
     */
    public function remove(Aktivita $aktivita);

    
    /**
     * Uloží upravenou aktivitu
     * @param Aktivita $aktivita
     * @return boolean
     */
    public function update(Aktivita $aktivita);
}