<?php

namespace common\models\dao;

use common\models\entity\Lek;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Lek
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:16
 */
class LekDAO implements ILekDAO
{

    /**
     * Uloží nový lék
     * @param Lek $lek Lék k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Lek $lek)
    {
        return $lek->save(false);
    }

    /**
     * Vrátí všechny leky
     * @return ArrayCollection<Lek> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(Lek::find()->all());
    }

    /**
     * Vrátí lék podle jejího ID.
     * @param integet $id ID hledaného léku
     * @return Lek Vrací nalezený lék, nebo null
     */
    public function getOneById($id)
    {
        return Lek::findOne(['id' => $id]);
    }

    /**
     * Vymaže lek
     * @param Lek $lek Lék ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Lek $lek)
    {
        return $lek->delete();
    }

    /**
     * Uloží upravený lek
     * @param Lek $lek Upravený lék k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Lek $lek)
    {
        return $lek->save(false);
    }
}