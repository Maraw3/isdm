<?php

namespace common\models\dao;

use common\models\entity\Operace;

/**
 * Data access object pro Osetreni
 * 
 * @author bruanmar
 * @version 1.0
 * @created 15-XII-2015 16:11:11
 */
class OsetreniDAO implements IOsetreniDAO
{

    /**
     * Uloží nové ošetření
     * @param Osetreni $osetreni Ošetření k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Osetreni $osetreni)
    {
        return $osetreni->save(false);
    }

    /**
     * Vrátí všechny ošetření
     * @return ActiveQuery Vrací všechny záznamy
     */
    public function getAll()
    {
        return Osetreni::find();
    }

    /**
     * Vrátí ošetření podle jejího ID.
     * @param integet $id ID hledaného ošetřeníu
     * @return ActiveRecord Vrací nalezený ošetření, nebo null
     */
    public function getOneById($id)
    {
        return Osetreni::findOne(['id' => $id]);
    }

    /**
     * Vymaže lek
     * @param Osetreni $osetreni Ošetření ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Osetreni $osetreni)
    {
        return $osetreni->delete();
    }

    /**
     * Uloží upravené ošetření
     * @param Osetreni $osetreni Upravené ošetření k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Osetreni $osetreni)
    {
        return $osetreni->save(false);
    }

}
