<?php

namespace common\models\dao;

use common\models\entity\Operace;

/**
 * Interface pro Data access object pro Osetreni
 * 
 * @author Quest
 * @version 1.0
 * @created 15-XII-2015 16:11:12
 */
interface IOsetreniDAO
{

    /**
     * Uloží nové ošetření
     * @param Osetreni $osetreni Ošetření k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Osetreni $osetreni);

    public function getAll();

    /**
     * 
     * @param id
     */
    public function getOneByID(int $id);

    /**
     * 
     * @param osetreni
     */
    public function remove(Osetreni $osetreni);

    /**
     * 
     * @param osetreni
     */
    public function update(Osetreni $osetreni);
}

?>