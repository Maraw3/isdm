<?php

namespace common\models\dao;

use common\models\entity\Rezident;
use yii\base\Object;

class RezidentDao extends Object implements IRezidentDao
{
    public function getAll()
    {
        return Rezident::find()->with('osoba');
    }
    
    public function getOneById($id)
    {
        return Rezident::findOne(['id' => $id]);
    }
    
    public function add(Rezident $rezident)
    {
        return $rezident->save();
    }
    
    public function update(Rezident $rezident)
    {
        return $rezident->save();
    }
    
    public function remove(Rezident $rezident)
    {
        return $rezident->delete();
    }
}