<?php

namespace common\models\dao;

use common\models\entity\Rezervace;
use common\components\collections\ArrayCollection;

/**
 * Data access object pro Rezervace
 * 
 * @author braunmar
 * @version 1.0
 * @created 15-XII-2015 16:11:08
 */
class RezervaceDAO implements IRezervaceDAO
{

    /**
     * Uloží novou rezervaci
     * @param Rezervace $rezervace Rezervace k uložení
     * @return boolean Vrací true, pokud byl záznam uspěšně uložen
     */
    public function add(Rezervace $rezervace)
    {
        return $rezervace->save(false);
    }

    /**
     * Vrátí všechny rezervace
     * @return ArrayCollection<Rezervace> Vrací všechny záznamy
     */
    public function getAll()
    {
        return new ArrayCollection(Rezervace::find()->all());
    }

    /**
     * Vrátí rezervaci podle jejího ID.
     * @param integer $id ID hledané rezervace
     * @return Rezervace Vrací nalezený rezervaci, nebo null
     */
    public function getOneById($id)
    {
        return Rezervace::findOne([
            'id' => $id
        ]);
    }

    /**
     * Vymaže rezervaci
     * @param Rezervace $rezervace Rezervace ke smazání
     * @return boolean Vratí true, pokud byl záznam smazán
     */
    public function remove(Rezervace $rezervace)
    {
        return $rezervace->delete();
    }

    /**
     * Uloží upravenou rezervaci
     * @param Rezervace $rezervace Upravená rezervaci k uložení
     * @return boolean Vrátí true, pokud byl záznam upraven
     */
    public function update(Rezervace $rezervace)
    {
        return $rezervace->save(false);
    }

}
