<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entity\Rezident;

/**
 * RezidentSearch represents the model behind the search form about `common\models\entity\Rezident`.
 */
class RezidentSearch extends Rezident
{
    /**
     * @inheritdoc
     */
    public $filtersActive = false;
    
    /**
     * @inheritdoc
     */
    public $globalSearch;
    
    /**
     * @inheritdoc
     */
    public $perPageValue = 10;
    
    /**
     * @inheritdoc
     */
    public $perPageName = 'per-page';
     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['globalSearch'], 'safe'],
            [['zaplaceno_do', 'nastup'], 'safe'],
            [['cislo_pokoje', 'osoba_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rezident::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => empty($params[$this->perPageName]) ? $this->perPageValue : $params[$this->perPageName],           
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        if (!empty($this->globalSearch)) {
                        return $dataProvider;
        }
        
        if(strlen(implode('', $params['RezidentSearch'])) > 0) {
            $this->filtersActive = true;
        }
        
        $query->orFilterWhere([
            'zaplaceno_do' => $this->zaplaceno_do,
            'nastup' => $this->nastup,
            'cislo_pokoje' => $this->cislo_pokoje,
            'osoba_id' => $this->osoba_id,
        ]);

        return $dataProvider;
    }
}
