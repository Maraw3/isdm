<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entity\Pokoj;

/**
 * PokojSearch represents the model behind the search form about `common\models\entity\Pokoj`.
 */
class PokojSearch extends Pokoj
{

    /**
     * @inheritdoc
     */
    public $filtersActive = false;

    /**
     * @inheritdoc
     */
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public $perPageValue = 10;

    /**
     * @inheritdoc
     */
    public $perPageName = 'per-page';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['globalSearch'], 'safe'],
            [['cislo_pokoje', 'kapacita'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pokoj::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => empty($params[$this->perPageName]) ? $this->perPageValue : $params[$this->perPageName],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->globalSearch)) {
            $query->orFilterWhere(['cislo_pokoje' => $this->globalSearch])
                    ->orFilterWhere(['kapacita' => $this->globalSearch]);
            return $dataProvider;
        }

        if (strlen(implode('', $params['PokojSearch'])) > 0) {
            $this->filtersActive = true;
        }

        $query->orFilterWhere([
            'cislo_pokoje' => $this->cislo_pokoje,
            'kapacita' => $this->kapacita,
        ]);

        return $dataProvider;
    }

}
