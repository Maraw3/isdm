<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entity\Lek;

/**
 * LekSearch represents the model behind the search form about `common\models\entity\Lek`.
 */
class LekSearch extends Lek
{
    /**
     * @inheritdoc
     */
    public $filtersActive = false;
    
    /**
     * @inheritdoc
     */
    public $globalSearch;
    
    /**
     * @inheritdoc
     */
    public $perPageValue = 10;
    
    /**
     * @inheritdoc
     */
    public $perPageName = 'per-page';
     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['globalSearch'], 'safe'],
            [['id', 'cena'], 'integer'],
            [['nazev', 'davkovani'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lek::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => empty($params[$this->perPageName]) ? $this->perPageValue : $params[$this->perPageName],           
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        if (!empty($this->globalSearch)) {
            $query->orFilterWhere(['like', 'nazev', $this->globalSearch])
            ->orFilterWhere(['like', 'davkovani', $this->globalSearch]);
            return $dataProvider;
        }
        
        if(strlen(implode('', $params['LekSearch'])) > 0) {
            $this->filtersActive = true;
        }
        
        $query->orFilterWhere([
            'id' => $this->id,
            'cena' => $this->cena,
        ]);

        $query->orFilterWhere(['like', 'nazev', $this->nazev])
            ->orFilterWhere(['like', 'davkovani', $this->davkovani]);

        return $dataProvider;
    }
}
