<?php

namespace common\components\behaviours;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class DateFormatBehaviour extends Behavior
{

    public $dateLogical = 'd.m.Y';
    public $datePsychical = 'Y-m-d';
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    public function afterFind()
    {
        foreach ($this->attributes as $attr) {
            $date = \DateTime::createFromFormat($this->datePsychical, $this->owner->{$attr});
            if (is_object($date)) {
                $this->owner->{$attr} = $date->format($this->dateLogical);
            }
        }
    }

    public function beforeValidate()
    {
        foreach ($this->attributes as $attr) {
            $date = \DateTime::createFromFormat($this->dateLogical, $this->owner->{$attr});

            if (is_object($date)) {
                $this->owner->{$attr} = $date->format($this->datePsychical);
            }
        }
    }

}
