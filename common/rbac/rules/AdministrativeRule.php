<?php

namespace common\rbac\rules;

use Yii;
use yii\rbac\Rule;

/**
 * Class for RBAC administrative role
 */
class AdministrativeRule extends Rule
{
    public $name = 'administrativa';

    public function execute($user, $item, $params)
    {
       return true;
    }
}