<?php

namespace common\rbac\rules;

use Yii;
use yii\rbac\Rule;

/**
 * Class for RBAC administrative role
 */
class LekarRule extends Rule
{
    public $name = 'lekar';

    public function execute($user, $item, $params)
    {
       return true;
    }
}