<?php

use yii\widgets\Menu;
use frontend\components\routes\SiteRoute;
use frontend\components\routes\PokojRoute;
use frontend\components\routes\RezidentRoute;

$user = \Yii::$app->getUser();

echo Menu::widget([
    'options' => [
        'class' => 'sidebar-menu'
    ],
    'items' => [
        [
            'label' => 'HLAVNÍ NAVIGACE',
            'options' => [
                'class' => 'header',
            ],
        ],
        [
            'label' => '<i class="fa fa-dashboard"></i><span>Hlavní stránka</span>',
            'url' => SiteRoute::home(),
            'active' => Yii::$app->controller->id == 'homepage',
        ],
        [
            'label' => '<i class="fa fa-user"></i><span>Rezidenti</span>',
            'url' => RezidentRoute::index(),
            'active' => Yii::$app->controller->id == 'rezident',
            'visible' => $user->can('list-rezident'),
        ],
        [
            'label' => '<i class="fa fa-medkid"></i><span>Léky</span>',
            'url' => '',
            'active' => Yii::$app->controller->id == 'Medicine',
            'visible' => $user->can('list-medicine'),
        ],
        [
            'label' => '<i class="fa fa-hotel"></i><span>Pokoje</span>',
            'url' => PokojRoute::index(),
            'active' => Yii::$app->controller->id == 'pokoj',
            'visible' => $user->can('list-room'),
        ],
        [
            'label' => 'Akce',
            'options' => [
                'class' => 'header',
            ],
        ],
        [
            'label' => '<i class="fa fa-long-arrow-right"></i><span>Přidělit pokoj</span>',
            'url' => RezidentRoute::accomodate(),
            'active' => Yii::$app->controller->action == 'accomodate',
            'visible' => $user->can('accomodate-rezident'),
        ]
    ],
    'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => false,
    'activateParents' => true,
]);