<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

use frontend\components\routes\ProfileRoute;
use frontend\components\routes\SiteRoute;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$userName = Yii::$app->user->identity->email;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">STD</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">Role</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?= Html::img('@web/img/avatars/avatar-default.png', ['alt' => 'User Image', 'class' => 'user-image']) ?>
                                    <span class="hidden-xs"><?= $userName ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <?= Html::img("@web/img/avatars/avatar-default.png", ['alt' => 'User Image', 'class' => 'img-circle']) ?>
                                        <p>
                                            <?= $userName ?>
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                        <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                        <?= Html::a(\Yii::t('app', 'Profile'), ProfileRoute::index(), ['class' => 'btn btn-default btn-flat']) ?>
                                        </div>
                                        <div class="pull-right">
                                        <?= Html::a(\Yii::t('app', 'Sign out'), SiteRoute::logout(), ['class' => 'btn btn-default btn-flat']) ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><span class="fa fa-gears"></span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                   <?= $this->render('_menu.php') ?>
                </section>
                <!-- /.sidebar -->
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?=
                    Breadcrumbs::widget([
                        'tag' => 'ol',
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </section>
                <section class="content">
                    <?= $content ?>
                </section>
            </div>
        </div>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
