<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Lek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lek-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nazev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'davkovani')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cena')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
