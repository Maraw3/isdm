<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\LekSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gender-search dataTables_filter form-inline">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax' => ''],
    ]); ?>

     <?php $field = $form->field($model, 'globalSearch', [
        'template' => '{input}',
    ]) ?>
    
    <?php $field->begin() ?>

    <label>
        <?= Yii::t('gridView', 'Search:') ?>
        <?= Html::activeTextInput($model, 'globalSearch', [
            'class' => 'form-control input-sm',
        ]) ?>
    </label>
    
    <?= Html::submitButton('<span class="fa fa-search"></span>', ['class' => 'btn btn-flat btn-primary btn-sm']) ?>

    <?php $field->end() ?>

    <?php ActiveForm::end() ?>
</div>
