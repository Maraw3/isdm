<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entity\Lek */

$this->title = Yii::t('app', 'Create lek');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Leks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lek-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
