<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\components\routes\PokojRoute;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Rezident */

$this->title = 'Rezident - ' . $model->osoba_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rezident'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rezident-view">
    <div class="btn-container margin-bottom">
        <?= Html::a(Yii::t('models', 'Update'), ['update', 'id' => $model->osoba_id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?=
        Html::a(Yii::t('models', 'Delete'), ['delete', 'id' => $model->osoba_id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('models', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </div>
    <div class="box box-primary">
        <div class="box-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'osoba.jmeno',
                    'osoba.prijmeni',
                    'osoba.email',
                    'zaplaceno_do:date',
                    'nastup:date',
                    [
                        'attribute' => 'cislo_pokoje',
                        'format' => 'raw',
                        'value' => Html::a($model->cislo_pokoje, PokojRoute::view($model->cislo_pokoje))
                    ],
                ],
            ])
            ?>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Adresa</h2>
        </div>
        <div class="box-body">
            <?php if ($model->osoba->adresa == null): ?>
                <p>Není evidována žádná adresa</p>
            <?php else: ?>
                <?=
                DetailView::widget([
                    'model' => $model->osoba->adresa,
                    'attributes' => [
                        'osoba.adresa.mesto',
                        'osoba.adresa.ulice',
                        'osoba.adresa.popisne_cislo',
                        'osoba.adresa.orientacni_cislo',
                        'osoba.adresa.psc',
                    ],
                ])
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>