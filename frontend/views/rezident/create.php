<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Rezident */

$this->title = Yii::t('app', 'Vytvořit rezidena');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rezident'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rezident-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'rooms' => $rooms,
    ])
    ?>
</div>
