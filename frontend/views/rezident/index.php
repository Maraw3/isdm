<?php

use yii\helpers\Html;
use yii\grid\GridView;
use nterms\pagesize\PageSize;
use common\components\widgets\FilterShowControl;
use yii\widgets\Pjax;
use frontend\components\routes\PokojRoute;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RezidentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rezidents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rezident-index">
    <div class="btn-container margin-bottom">
        <?= Html::a(Yii::t('app', 'Vytvořit rezidenta'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box">
        <div class="box-body">
            <?php  Pjax::begin(['id' => 'rezident-pjax']); ?>
            <div class="row">
                <div class="col-sm-6 form-inline dataTables_length">
                    <?=PageSize::widget([
                        'beforeLabel' => Yii::t('gridView', 'Showing'),
                        'afterLabel' => Yii::t('gridView', 'entries'),
                        'defaultPageSize' => $searchModel->perPageValue,
                        'options' => ['class' => 'form-control input-sm'],
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->render('_search', ['model' => $searchModel]) ?>
                </div>
            </div>
            <?= GridView::widget([
                'id' => 'gridview-rezident',
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered table-striped table-hover dataTable table-actions'],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    $class = $index % 2 ? 'odd' : 'even';
                    return ['class' => $class];
                },
                'summary' => Yii::t('gridView', 'Showing') . ' {begin} ' . Yii::t('gridView', 'to') . ' {end} ' . Yii::t('gridView', 'of') . ' {totalCount} ' . Yii::t('gridView', 'entries'),
                'layout' => '{items}'
                        . '<div class="row">'
                            . '<div class="col-sm-5">'
                                . '<div class="dataTables_info">{summary}</div>'
                                . '<div>' . FilterShowControl::widget(['filtersId' =>'gridview-rezident-filters']) .'</div>'
                            . '</div>'
                            . '<div class="col-sm-7">'
                                . '<div class="dataTables_paginate paging_simple_numbers">{pager}</div>'
                            . '</div>'
                        . '</div>',
                'filterSelector' => 'select[name="' . $searchModel->perPageName . '"]',
                'pager' => [
                    'nextPageLabel' => Yii::t('gridView', 'Next'),  
                    'prevPageLabel' => Yii::t('gridView', 'Previous'),
                ],
                'filterModel' => $searchModel,
                'columns' => [
            
                    'osoba.jmeno',
                    'osoba.prijmeni',
                    [
                        'label' => 'Číslo pokoje',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Html::a($data->cislo_pokoje, PokojRoute::view($data->cislo_pokoje));
                        }
                    ],
                    'nastup:date',
                    'zaplaceno_do:date',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
