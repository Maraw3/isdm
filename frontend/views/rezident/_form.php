<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Rezident */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rezident-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Údaje o osobě</h2>
        </div>
        <div class="box-body">
            <?= $form->field($model->osoba, 'jmeno')->textInput() ?>

            <?= $form->field($model->osoba, 'prijmeni')->textInput() ?>

            <?= $form->field($model->osoba, 'rodneCislo')->textInput() ?>

            <?= $form->field($model->osoba, 'email')->textInput() ?>
            <?php if ($model->isNewRecord) {
                echo $form->field($model->osoba, 'heslo_hash')->passwordInput();
            }?>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title bordered">Údaje o rezidentovi</h2>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'zaplaceno_do')->widget(DatePicker::className(), [
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy'
                ]
            ]) ?>

            <?= $form->field($model, 'nastup')->widget(DatePicker::className(), [
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy'
                ]
            ]) ?>

            <?= $form->field($model, 'cislo_pokoje')->dropDownList($rooms, ['prompt' => 'Vyberte pokoj']) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('models', 'Create') : Yii::t('models', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
