<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Rezident */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'rezident',
        ]) . ' ' . $model->osoba_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rezidents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->osoba_id, 'url' => ['view', 'id' => $model->osoba_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rezident-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'rooms' => $rooms,
    ])
    ?>
</div>
