<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Rezident */

$this->title = Yii::t('app', 'Přiřazení rezidenta na pokoj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rezident'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rezident-create">
    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin() ?>

            <?= $form->field($model, 'rezident')->dropDownList($model->rezidents) ?>

            <?= $form->field($model, 'pokoj')->dropDownList($model->rooms) ?>

            <?= Html::submitButton(Yii::t('app', 'Provést'), ['class' => 'btn btn-primary btn-flat']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
