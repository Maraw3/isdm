<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title =  \Yii::t('app', 'Administrace');
$this->params['breadcrumbs'][] = $this->title;
?>
             
        <?php $form = ActiveForm::begin([
            'id' => false,
            'enableClientValidation' => false,
        ]); ?>
        <?= $form->field($model, 'username', [
            'template' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>',
            'options' => [
                'class' => 'has-feedback form-group',
            ]
        ])->textInput([
            'placeholder' => 'Username',
        ]) ?>
        <?= $form->field($model, 'password', [
            'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
            'options' => [
                'class' => 'has-feedback form-group',
            ],
        ])->passwordInput([
            'placeholder' => 'Password',
        ]) ?>
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->widget(CheckboxX::className(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'autoLabel' => true,
                    'pluginOptions' => [ 'threeState' => false, 'size' => 'sm', 'enclosedLabel' => true ],
                    'options' => ['value' => 0],
                ])->label(false) ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </section>
</div>
