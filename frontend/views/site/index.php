<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Login';
?>

<div class="login-box">
    <div class="login-logo">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <section class="login-box-body">
<?= $this->render('_form', ['model' => $model]) ?>
    </section>
    <div class="tips">
        <p style="font-size: 23px;">visor@visor.cz</p>
        <p style="font-size: 23px;">test</p>
    </div>
</div>