<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Pokoj */

$this->title = Yii::t('app', 'Vytvořit pokoj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pokoj'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pokoj-create">
    <div class="box">
        <div class="box-body">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>
