<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Pokoj */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'pokoj',
]) . ' ' . $model->cislo_pokoje;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pokoj'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cislo_pokoje, 'url' => ['view', 'id' => $model->cislo_pokoje]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pokoj-update">
    <div class="box box-primary">
        <div class="box-body">
            
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
            
        </div>
    </div>
</div>
