<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Pokoj */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pokoj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cislo_pokoje')->textInput() ?>

    <?= $form->field($model, 'kapacita')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Vytvořit') : Yii::t('app', 'Upravit'), ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
