<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Pokoj */

$this->title = 'Pokoj - ' . $model->cislo_pokoje;
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Pokoj'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pokoj-view">
    <div class="btn-container margin-bottom">
        <?= Html::a(Yii::t('models', 'Update'), ['update', 'id' => $model->cislo_pokoje], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('models', 'Delete'), ['delete', 'id' => $model->cislo_pokoje], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('models', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box box-primary">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                  'cislo_pokoje',
                  'kapacita',
                ],
            ]) ?>
        </div>
    </div>
</div>
