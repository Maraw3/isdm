<?php

use yii\helpers\Html;
use yii\grid\GridView;
use nterms\pagesize\PageSize;
use common\components\widgets\FilterShowControl;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PokojSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pokoj');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pokoj-index">
    <div class="btn-container margin-bottom">
        <?= Html::a('Vytvoř pokoj', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box">
        <div class="box-body">
            <?php  Pjax::begin(['id' => 'pokoj-pjax']); ?>
            <div class="row">
                <div class="col-sm-6 form-inline dataTables_length">
                    <?= PageSize::widget([
                        'beforeLabel' => Yii::t('gridView', 'Showing'),
                        'afterLabel' => Yii::t('gridView', 'entries'),
                        'defaultPageSize' => $searchModel->perPageValue,
                        'options' => ['class' => 'form-control input-sm'],
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->render('_search', ['model' => $searchModel]) ?>
                </div>
            </div>
            <?= GridView::widget([
                'id' => 'gridview-room',
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered table-striped table-hover dataTable table-actions'],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    $class = $index % 2 ? 'odd' : 'even';
                    return ['class' => $class];
                },
                'summary' => Yii::t('gridView', 'Zobrazeno') . ' {begin} ' . Yii::t('gridView', 'do') . ' {end} ' . Yii::t('gridView', 'z') . ' {totalCount} ' . Yii::t('gridView', 'položek'),
                'layout' => '{items}'
                        . '<div class="row">'
                            . '<div class="col-sm-5">'
                                . '<div class="dataTables_info">{summary}</div>'
                                . '<div>' . FilterShowControl::widget(['filtersId' =>'gridview-room-filters', 'label' => 'Rozšířené hledání']) .'</div>'
                            . '</div>'
                            . '<div class="col-sm-7">'
                                . '<div class="dataTables_paginate paging_simple_numbers">{pager}</div>'
                            . '</div>'
                        . '</div>',
                'filterSelector' => 'select[name="' . $searchModel->perPageName . '"]',
                'pager' => [
                    'nextPageLabel' => Yii::t('gridView', 'Next'),  
                    'prevPageLabel' => Yii::t('gridView', 'Previous'),
                ],
                'filterModel' => $searchModel,
                'columns' => [
            
                    'cislo_pokoje',
                    'kapacita',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
