<?php
namespace frontend\controllers;

use Yii;

/**
 * Homepage (after login) controller
 */
class HomepageController extends SecureController
{    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }  
}
