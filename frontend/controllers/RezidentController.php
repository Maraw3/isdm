<?php

namespace frontend\controllers;

use Yii;
use common\models\entity\Rezident;
use common\models\entity\Osoba;
use common\models\search\RezidentSearch;
use common\services\operations\IAdministrativeOperations;
use common\models\forms\AccomodateForm;
use frontend\controllers\SecureController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * RezidentController implements the CRUD actions for Rezident model.
 */
class RezidentController extends SecureController
{

    /**
     * Operace pro administraci
     * @var IAdministrativeOperations
     */
    private $administrativeOperations;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rezident models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!$this->user->can('list-rezident')) {
            $this->throwForbiddenException();
        }
        
        $searchModel = new RezidentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rezident model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!$this->user->can('view-rezident')) {
            $this->throwForbiddenException();
        }
        
        return $this->render('view', [
             'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rezident model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!$this->user->can('create-rezident')) {
            $this->throwForbiddenException();
        }

        $osoba = \Yii::createObject(Osoba::className(), [
            'scenario' => Osoba::SCENARIO_CREATE,
        ]);
        
        $model = new Rezident();
        $model->osoba = $osoba;

        if ($model->load(Yii::$app->request->post()) && $model->osoba->load(Yii::$app->request->post()) &&
                $this->administrativeOperations->addRezident($model)) {
            return $this->redirect(['view', 'id' => $model->osoba_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'rooms' => ArrayHelper::map($this->administrativeOperations->getFreePokoj()->toArray(), 'cislo_pokoje', 'cislo_pokoje'),
            ]);
        }
    }

    /**
     * Updates an existing Rezident model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        if (!$this->user->can('update-rezident')) {
            $this->throwForbiddenException();
        }
        
        $model = $this->findModel($id);
        $model->osoba->scenario = Osoba::SCENARIO_UPDATE;
        
        if ($model->load(Yii::$app->request->post()) && $model->osoba->load(Yii::$app->request->post()) &&
                $this->administrativeOperations->updateRezident($model)) {
            return $this->redirect(['view', 'id' => $model->osoba_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'rooms' => ArrayHelper::map($this->administrativeOperations->getFreePokoj()->toArray(), 'cislo_pokoje', 'cislo_pokoje'),
            ]);
        }
    }

    /**
     * Deletes an existing Rezident model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!$this->user->can('delete-rezident')) {
            $this->throwForbiddenException();
        }
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionAccomodate()
    {
        if (!$this->user->can('accomodate-rezident')) {
            $this->throwForbiddenException();
        }
        
        /** @var AccomodateForm $model */
        $model = Yii::createObject('models.forms.accomodate');
        
        if ($model->load(Yii::$app->request->post()) && $model->accomodate()) {
            Yii::$app->session->setFlash('info', 'Zámnam byl upraven');
        }
        
        return $this->render('accomodate', [
           'model' => $model,
        ]);
    }

    /**
     * Finds the Rezident model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rezident the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->administrativeOperations->getOneByIdRezident($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Nastaví administrative operations (DI)
     * @param IAdministrativeOperations $ao
     */
    public function setAdministrativeOperations(IAdministrativeOperations $ao)
    {
        $this->administrativeOperations = $ao;
    }

}
