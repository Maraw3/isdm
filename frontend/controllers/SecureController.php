<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Secure controller
 */
class SecureController extends Controller
{
    protected $layoutName = 'admin';
    
    /**
     * @var \yii\web\User
     */
    protected $user;

    public function init()
    {
        parent::init();
        $this->user = \Yii::$app->getUser();
        $this->layout = $this->layoutName;
    }
    
    protected function throwForbiddenException($message = 'You have no permission for this action')
    {
        throw new ForbiddenHttpException($message);
    }
}
