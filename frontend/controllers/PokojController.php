<?php

namespace frontend\controllers;

use Yii;
use common\models\entity\Pokoj;
use common\models\search\PokojSearch;
use common\services\operations\IAdministrativeOperations;
use frontend\controllers\SecureController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PokojController implements the CRUD actions for Pokoj model.
 */
class PokojController extends SecureController
{
    /**
     * Operace pro administraci
     * @var IAdministrativeOperations
     */
    private $administrativeOperations;
    
    /**
     * Chování controlleru
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pokoj models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!$this->user->can('list-room')) {
            $this->throwForbiddenException();
        }
        
        $searchModel = new PokojSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pokoj model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!$this->user->can('view-room')) {
            $this->throwForbiddenException();
        }
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pokoj model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!$this->user->can('create-room')) {
            $this->throwForbiddenException();
        }
        
        $model = new Pokoj();

        if ($model->load(Yii::$app->request->post()) && $this->administrativeOperations->addPokoj($model)) {
            return $this->redirect(['view', 'id' => $model->cislo_pokoje]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pokoj model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!$this->user->can('update-room')) {
            $this->throwForbiddenException();
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->administrativeOperations->updatePokoj($model)) {
            return $this->redirect(['view', 'id' => $model->cislo_pokoje]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pokoj model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!$this->user->can('delete-room')) {
            $this->throwForbiddenException();
        }
        
        $pokoj = $this->findModel($id);
        
        $this->administrativeOperations->deletePokoj($pokoj);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pokoj model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pokoj the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->administrativeOperations->getOneByIdPokoj($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Nastaví administrative operations (DI)
     * @param IAdministrativeOperations $ao
     */
    public function setAdministrativeOperations(IAdministrativeOperations $ao)
    {
        $this->administrativeOperations = $ao;
    }
}
