<?php
$manager = [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    //'enableStrictParsing' => true,
    'showScriptName' => false,
    //'suffix' => '/',
    'rules' => frontend\components\helpers\url\UrlRuleHelper::getRulesDefinitions(),
    //'baseUrl' => false,
];

return array_merge(
    $manager, require(__DIR__.'/managers-local.php')
);

