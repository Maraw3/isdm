<?php

use frontend\components\helpers\url\UrlRuleHelper;
use frontend\components\rules\url\SiteUrlRule;
use frontend\components\rules\url\CRUDUrlRule;
use frontend\components\rules\url\RezidentUrlRule;

$rules = [
    /**
     * =================
     * Frontend rules
     * =================
     */
    [
        'class' => SiteUrlRule::className(),
        'pattern' => SiteUrlRule::PATTERN_DEFAULT,
        'route' => SiteUrlRule::ROUTE_DEFAULT,
        'verb' => ['GET', 'POST'],
    ],
    [
        'class' => SiteUrlRule::className(),
        'pattern' => SiteUrlRule::PATTERN_LOGOUT,
        'route' => SiteUrlRule::ROUTE_LOGOUT,
        'verb' => ['GET'],
    ],
    /**
     * =================
     * CRUD rules
     * =================
     */
    [
        'class' => CRUDUrlRule::className(),
        'pattern' => CRUDUrlRule::PATTERN_DEFAULT,
        'route' => CRUDUrlRule::ROUTE_DEFAULT,
        'verb' => ['GET'],
    ],
    [
        'class' => CRUDUrlRule::className(),
        'pattern' => CRUDUrlRule::PATTERN_CREATE,
        'route' => CRUDUrlRule::ROUTE_CREATE,
        'verb' => ['GET', 'POST'],
    ],
    [
        'class' => CRUDUrlRule::className(),
        'pattern' => CRUDUrlRule::PATTERN_VIEW,
        'route' => CRUDUrlRule::ROUTE_VIEW,
        'verb' => ['GET'],
    ],
    [
        'class' => CRUDUrlRule::className(),
        'pattern' => CRUDUrlRule::PATTERN_UPDATE,
        'route' => CRUDUrlRule::ROUTE_UPDATE,
        'verb' => ['GET', 'POST'],
    ],
    [
        'class' => CRUDUrlRule::className(),
        'pattern' => CRUDUrlRule::PATTERN_DELETE,
        'route' => CRUDUrlRule::ROUTE_DELETE,
        'verb' => ['GET', 'POST'],
    ],
    /**
     * =================
     * Operations rules
     * =================
     */
     [
        'class' => RezidentUrlRule::className(),
        'pattern' => RezidentUrlRule::PATTERN_ACCOMODATE,
        'route' => RezidentUrlRule::ROUTE_ACCOMODATE,
        'verb' => ['GET', 'POST'],
    ],
];

return array_merge(
        $rules, require(__DIR__ . '/rules-local.php')
);
