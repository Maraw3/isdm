<?php

namespace frontend\components\helpers\url;

class UrlManagerHelper
{
    /**
     * Managers path
     */
    const PATH_ALIAS_MANAGER = '@frontend/config/url/managers.php';

    /**
     * Retrieves url manager definitions
     * @return array url rules
     */
    public static function getManagerDefinitions()
    {
        return require(\Yii::getAlias(self::PATH_ALIAS_MANAGER));
    }
}