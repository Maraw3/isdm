<?php

namespace frontend\components\helpers\url;

use yii\helpers\ArrayHelper;

class UrlRuleHelper {

    /**
     * Rulest path
     */
    const PATH_ALIAS_RULES = '@frontend/config/url/rules.php';
    const PATH_ALIAS_HOSTS = '@frontend/config/url/hosts.php';
    
    /**
     * Hosts
     */
    const HOST_WWW     = 'www';
    const HOST_SCHOOL  = 'school';
    const HOST_STUDENT = 'student';

    /**
     * Retrieves url rules definitions
     * @return array url rules
     */
    public static function getRulesDefinitions()
    {
        return require(\Yii::getAlias(self::PATH_ALIAS_RULES));
    }
    
    /**
     * Retrieves hosts definitions
     * @return array hosts definitions
     */
    public static function getHostsDefinitions()
    {
        return require(\Yii::getAlias(self::PATH_ALIAS_HOSTS));
    }

    /**
     * Retrieves host definition
     * @param string $type host type
     */
    public static function getHostDefinition($type)
    {
        $definitions = self::getHostsDefinitions();

        return ArrayHelper::getValue($definitions, $type, null);
    }
}