<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class CRUDUrlRule extends UrlRule
{
    /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/<controller>/list';
    const PATTERN_VIEW = '/<controller>/<id>/view';
    const PATTERN_UPDATE = '/<controller>/<id>/update';
    const PATTERN_CREATE = '/<controller>/create';
    const PATTERN_DELETE = '/<controller>/<id>/delete';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/<controller>/index';
    const ROUTE_CREATE = '/<controller>/create';
    const ROUTE_UPDATE = '/<controller>/update';
    const ROUTE_VIEW = '/<controller>/view';
    const ROUTE_DELETE = '/<controller>/delete';
}