<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class HomepageUrlRule extends UrlRule
{
    /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/homepage/index';
}