<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class ProfileUrlRule extends UrlRule
{
    /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/profile';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/profile/index';
}