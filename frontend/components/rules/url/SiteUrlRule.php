<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class SiteUrlRule extends UrlRule
{
    /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/';
    const PATTERN_LOGIN = '/login';
    const PATTERN_LOGOUT = '/logout';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/site/index';
    const ROUTE_LOGIN = '/site/login';
    const ROUTE_LOGOUT = '/site/logout';

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        $get = $request->get();
        
        if (empty($pathInfo)) {
            if (!\Yii::$app->user->isGuest) {
                return [HomepageUrlRule::ROUTE_DEFAULT, $get];
            }
            
            return [self::ROUTE_DEFAULT, $get];
        }
        
        return parent::parseRequest($manager, $request);
    }
}