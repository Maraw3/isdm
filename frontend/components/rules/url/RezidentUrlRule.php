<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class RezidentUrlRule extends UrlRule
{
     /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/rezident/list';
    const PATTERN_CREATE = '/rezident/create';
    const PATTERN_UPDATE = '/rezident/{id}/update';
    const PATTERN_VIEW = '/rezident/{id}/view';
    const PATTERN_DELETE = '/rezident/{id}/delete';
    const PATTERN_ACCOMODATE = '/rezident/accomodate';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/rezident/index';
    const ROUTE_UPDATE = '/rezident/update';
    const ROUTE_VIEW = '/rezident/view';
    const ROUTE_CREATE = '/rezident/create';
    const ROUTE_DELETE = '/rezident/delete';
    const ROUTE_ACCOMODATE = '/rezident/accomodate';
}