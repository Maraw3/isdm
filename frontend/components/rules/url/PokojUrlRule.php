<?php

namespace frontend\components\rules\url;

use yii\web\UrlRule;

class PokojUrlRule extends UrlRule
{
    /*
     * Avaliable patterns
     */
    const PATTERN_DEFAULT = '/room/list';
    const PATTERN_CREATE = '/room/create';
    const PATTERN_UPDATE = '/room/{id}/update';
    const PATTERN_VIEW = '/room/{id}/view';
    const PATTERN_DELETE = '/room/{id}/delete';
    
    /*
     * Avaliable routes
     */
    const ROUTE_DEFAULT = '/pokoj/index';
    const ROUTE_UPDATE = '/pokoj/update';
    const ROUTE_VIEW = '/pokoj/view';
    const ROUTE_CREATE = '/pokoj/create';
    const ROUTE_DELETE = '/pokoj/delete';
}