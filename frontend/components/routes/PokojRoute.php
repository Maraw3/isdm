<?php

namespace frontend\components\routes;

use common\components\helpers\route\RouteHelper;
use frontend\components\rules\url\PokojUrlRule;

/**
 * This is the helper class for Pokoj routes.
 */
class PokojRoute extends RouteHelper
{

    /**
     * Retrieves index list route
     * @param array $params Additional route params
     * @return array route
     */
    public static function index(array $params = [])
    {
        return self::getRoute(PokojUrlRule::ROUTE_DEFAULT, $params);
    }

    /**
     * Retrieves room create route
     * @param array $params Additional route params
     * @return array route
     */
    public static function create(array $params = [])
    {
        return self::getRoute(PokojUrlRule::ROUTE_CREATE, $params);
    }

    /**
     * Retrieves view room route
     * @param array $params Additional route params
     * @return array route
     */
    public static function view($id, array $params = [])
    {
        return self::getRoute(PokojUrlRule::ROUTE_VIEW, array_merge(['id' => $id], $params));
    }

    /**
     * Retrieves room update route
     * @param integer $id Room id
     * @param array $params Additional route params
     * @return array route
     */
    public static function update($id, array $params = [])
    {
        return self::getRoute(PokojUrlRule::ROUTE_UPDATE, array_merge(['id' => $id], $params));
    }

    /**
     * Retrieves room delete route
     * @param array $params Additional route params
     * @return array route
     */
    public static function delete($id, array $params = [])
    {
        return self::getRoute(PokojUrlRule::ROUTE_DELETE, array_merge(['id' => $id], $params));
    }

}
