<?php

namespace frontend\components\routes;

use common\components\helpers\route\RouteHelper;
use frontend\components\rules\url\SiteUrlRule;

/**
 * This is the helper class for Site routes.
 */
class SiteRoute extends RouteHelper {

    /**
     * Retrieves index route
     * @param array $params Additional route params
     * @return array route
     */
    public static function index(array $params = [])
    {
        return self::getRoute(SiteUrlRule::ROUTE_DEFAULT, $params);
    }
    
    /**
     * Retrieves home route
     * @param array $params Additional route params
     * @return array route
     */
    public static function home(array $params = [])
    {
        return self::index($params);
    }
    
    /**
     * Retrieves login route
     * @param array $params Additional route params
     * @return array route
     */
    public static function login(array $params = [])
    {
        return self::getRoute(SiteUrlRule::ROUTE_LOGIN, $params);
    }
    
    /**
     * Retrieves logout route
     * @param array $params Additional route params
     * @return array route
     */
    public static function logout(array $params = [])
    {
        return self::getRoute(SiteUrlRule::ROUTE_LOGOUT, $params);
    }
}