<?php

namespace frontend\components\routes;

use common\components\helpers\route\RouteHelper;
use frontend\components\rules\url\ProfileUrlRule;

/**
 * This is the helper class for Profile routes.
 */
class ProfileRoute extends RouteHelper {

    /**
     * Retrieves user profile route
     * @param array $params Additional route params
     * @return array route
     */
    public static function index(array $params = [])
    {
        return self::getRoute(ProfileUrlRule::ROUTE_DEFAULT, $params);
    }
}