<?php

namespace frontend\components\routes;

use common\components\helpers\route\RouteHelper;
use frontend\components\rules\url\RezidentUrlRule;

/**
 * This is the helper class for Rezident routes.
 */
class RezidentRoute extends RouteHelper
{

    /**
     * Retrieves list route
     * @param array $params Additional route params
     * @return array route
     */
    public static function index(array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_DEFAULT, $params);
    }

    /**
     * Retrieves rezident create route
     * @param array $params Additional route params
     * @return array route
     */
    public static function create(array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_CREATE, $params);
    }

    /**
     * Retrieves view rezident route
     * @param array $params Additional route params
     * @return array route
     */
    public static function view($id, array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_VIEW, array_merge(['id' => $id], $params));
    }

    /**
     * Retrieves rezident update route
     * @param integer $id Rezident ID
     * @param array $params Additional route params
     * @return array route
     */
    public static function update($id, array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_UPDATE, array_merge(['id' => $id], $params));
    }

    /**
     * Retrieves rezident delete route
     * @param array $params Additional route params
     * @return array route
     */
    public static function delete($id, array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_DELETE, array_merge(['id' => $id], $params));
    }

    /**
     * Retrieves accomodate route
     * @param array $params Additional route params
     * @return array route
     */
    public static function accomodate(array $params = [])
    {
        return self::getRoute(RezidentUrlRule::ROUTE_ACCOMODATE, $params);
    }

}
