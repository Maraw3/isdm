//http://www.yiiframework.com/wiki/654/escape-from-default-s-yii2-delete-confirm-box/

yii.allowAction = function ($e) {
    var message = $e.data('confirm');
    return message === undefined || yii.confirm(message, $e);
};
yii.confirm = function (message, ok, cancel) {
    bootbox.confirm(message, function (confirmed) {
        if (confirmed) {
           if (confirmed) {
                !ok || ok();
            } else {
                !cancel || cancel();
            }
        }
    });
    // confirm will always return false on the first call
    // to cancel click handler
    return false;
}