(function() {
    $.Helpers = {};
    
    $.Helpers.showControl = function(widgetId, triggerId, toggleCssClass) {
        var $widget = $('#' + widgetId);
        var $trigger = $('#' + triggerId);
        
        /* $widget.find('input').each(function() {
           $(this).off('.yiiActiveForm');
        });*/
        
        if ($trigger.is(':checked')) {
            $widget.removeClass(toggleCssClass);
        }
        
        $(document).on('change', $trigger, function() {
            if ($trigger.is(':checked')) {
                $widget.removeClass(toggleCssClass);
            } else {
                $widget.addClass(toggleCssClass);  
            }
        });
    };
    
    $.Helpers.gridViewFilters = function(widgetId, filtersId, activeCssClass) {
        var $widget = $('#' + widgetId);
        var $filters = $('#' + filtersId);
        $widget.on('click', function() {
            $filters.toggleClass(activeCssClass);
        });
    };
    
    $.Helpers.gridViewLiveSearch = function() {
        var globalSearch = $('input[type="text"][name*="[globalSearch]"]');
        var grid = $('[id*="gridview"] > .table.dataTable');
        var interval;
        
        $(document).on('afterFilter', grid, function() {
            globalSearch.focus();
        });
        
        $(document).on('input', globalSearch, function() {
            clearTimeout(interval);
            interval = setTimeout(function() {
                grid.yiiGridView('applyGlobalFilter');
            }, 500);
        });
    };
    
    $.Helpers.updateQuery = function(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
          uri = uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
          uri = uri + separator + key + "=" + value;
        }
        history.pushState(null, document.title, uri);
      };
})();