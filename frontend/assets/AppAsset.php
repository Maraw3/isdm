<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    
    public $baseUrl = '@web';
    
    public $css = [
        'css/bootstrap.min.css',
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
        'css/font-awesome/css/font-awesome.css',
        'css/plugins/datatables/dataTables.bootstrap.css',
    ];
    public $js = [
        'js/jquery-ui.min.js',
        'js/app.min.js',
        'js/bootstrap.min.js',
        'js/helpers.js',
        'js/bootbox.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
